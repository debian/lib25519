lib25519 (0~20241004-1) unstable; urgency=medium

  * New upstream version 0~20241004
  * d/rules: re-enable crypto_hashblocks/sha512/wflip, fixed in upstream
  * d/p/0001-add-missing-include-build-crypto_declassify.h.patch: remove,
    fixed in upstream

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Sat, 05 Oct 2024 12:19:24 +0200

lib25519 (0~20240928-2) unstable; urgency=medium

  * d/rules: disable crypto_hashblocks/sha512/wflip implementation, fix
    lib25519-test on s390x/hppa/powerpc/ppc64/sparc64
  * d/t/lib25519-fulltest: skip the test if valgrind is not available,
    add Restrictions allow-stderr, skippable

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Tue, 01 Oct 2024 20:56:47 +0200

lib25519 (0~20240928-1) unstable; urgency=medium

  * New upstream version 0~20240928
  * d/p/0002-remove-internal-_C-symbols-from-.symbol-file.patch,
    d/p/0003-fix-missing-asm-hidden.patch, d/p/0100-FTBR-sort-ofiles.patch
    remove, fixed in upstream
  * d/rules: re-enable donna_c64 implementations
  * d/rules: remove optimize=+lto
  * d/rules: B-D on valgrind-if-available
  * d/rules: check if valgrind is available
  * d/p/0001-add-missing-include-build-crypto_declassify.h.patch add
  * d/t/test25519-shared: use dpkg-buildflags to get CFLAGS,LDFLAGS
  * d/t/test25519-static: use dpkg-buildflags to get CFLAGS,LDFLAGS
  * d/control: bump Standards-Version: 4.7.0, no changes
  * d/gbp.conf: dist = DEP14, debian-branch = debian/latest
  * d/t/lib25519-fulltest add
  * d/upstream/metadata add

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Mon, 30 Sep 2024 21:23:54 +0200

lib25519 (0~20240321-2) unstable; urgency=medium

  * d/rules: run test lib25519-test
  * d/rules: disable donna_c64 implementations, fix build/autopkgtest on s390x

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Wed, 24 Apr 2024 19:53:05 +0200

lib25519 (0~20240321-1) unstable; urgency=medium

  [ Simon Josefsson ]
  * New upstream version 0~20240321
  * Add ABIs to symbols files.
  * Add d/salsa-ci.yml for wrap-and-sort.

  [ Jan Mojžíš ]
  * d/copyright: bump lib25519 copyright years
  * d/p/0002-remove-internal-_C-symbols-from-.symbol-file.patch add
  * d/p/0003-fix-missing-asm-hidden.patch add
  * d/lib25519-1.symbols.{amd64,x32} remove
  * d/rules: remove manual selection and enable all implementation

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Mon, 22 Apr 2024 14:51:50 +0200

lib25519 (0~20230630-4) unstable; urgency=medium

  * d/copyright: bump debian/* copyright year
  * d/gbp.conf: add [pull] track-missing = True
  * d/control: remove 'Multi-Arch: same' for the package lib25519-dev,
    lib25519-dev contains the lib25519.h header file, which is different
    across platforms.

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Mon, 08 Jan 2024 09:31:53 +0100

lib25519 (0~20230630-3) unstable; urgency=medium

  * d/rules: select crypto_hashblocks/sha512/inplace implementation
    instead of crypto_hashblocks/sha512/wflip
  * d/control: add Multi-Arch: same

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Sun, 31 Dec 2023 18:52:43 +0100

lib25519 (0~20230630-2) unstable; urgency=medium

  * d/compiler/gcc: Echo commands for blhc.
  * d/rules add DEB_BUILD_MAINT_OPTIONS = optimize=+lto
  * d/watch: use special strings @PACKAGE@, @ANY_VERSION@, @ARCHIVE_EXT@
  * d/rules: detect the correct ABINAME from the DEB_BUILD_ARCH
    instead of run-time detection. Fixes FTBR on i386/armhf/...
  * d/p/0100-FTBR-sort-ofiles.patch: sort the output of os.listdir()
  * d/lib25519-1.symbols.x32: add symbolic link to lib25519-1.symbols.amd64
  * d/rules: enable selected AMD64 implementations

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Sat, 30 Dec 2023 14:57:06 +0100

lib25519 (0~20230630-1) unstable; urgency=medium

  * Initial release. (Closes: #1051553)

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Mon, 11 Sep 2023 17:29:24 +0200
